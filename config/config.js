

process.env.PORT = process.env.PORT || 3000

process.env.NODE_ENV = process.env.NODE_ENV || 'dev'

process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30

process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo'

let urlDB

if(process.env.NODE_ENV === 'dev'){
    urlDB = 'mongodb://localhost:27017/lab08'
}
else{
    urlDB = 'mongodb+srv://Renato:R3velation@cluster0-91ctr.mongodb.net/lab08'
}

process.env.URLDB = urlDB

process.env.CLIENT_ID = process.env.CLIENT_ID || '970611167737-vmpvppuijkoothkqnn6u3b5cfptm3amc.apps.googleusercontent.com'