const express = require("express");
const path = require("path");

const app = express();

app.use(require('./usuario'));
app.use(require('./login'));

app.use(require("./upload"));

app.use(require("./images"));


app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname+'/../Views/home.html'));
});


module.exports = app